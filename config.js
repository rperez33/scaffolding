var config = {
	server:{
		port : process.env.PORT || 8888
	},
 	db:{
 		port: process.env.MONGODB_URI || 'mongodb://localhost/wonderchat'
 	},
  	application: {
    	controllers: {
      	default: 'index',
      	current: ''
    	}
 	},
};
 
module.exports = config;