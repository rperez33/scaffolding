
var multer=require('multer');
var storage = multer.diskStorage({
	destination: (req,file,cb) => {
		cb(null,'./uploads')
	},
	filename: (req,file,cb) => {
		cb(null,file.fieldname + "-" + Date.now() + file.originalname)
	}
});
var upload = multer({
	storage:storage
}).single("file");

module.exports = upload;
